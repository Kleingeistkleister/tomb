import threading
import thread
import time

rFlag = 0   #shared between Thread_A and Thread_B
gFlag = 0
bFlag = 0
flags = [ rFlag , gFlag , bFlag ]
valrgb = 0.003921 #  % per RGB Value (0-255) 

#RGB Multiplier
rVal = 1
gVal = 1
bVal = 1
multiVAL = [rVal, gVal, bVal]
speed = 0.01  #10 ms

#LED
b = 22
r = 23
g = 24

LEDS = [r , g , b] 


def changer(pin, amount):
    f = open('/dev/pi-blaster', 'w')
    f.write('%d=%s\n'%(pin, str(amount))) 

def colour(r , g , b):
    changer(LEDS[0], r)
    changer(LEDS[1], g)
    changer(LEDS[2], b)
    

def setFlag(x):
	for i in LEDS:
	    flags[i] = 0
    
    
def blackout():
    colour(0 , 0 ,0 )
    setFlag(0)
	    

def strobe(name , f , k ):
    smoke(1)
    y = 0
    while y < f:
	colour(0 ,0 ,0)
	time.sleep(k)
	colour(1, 1, 1)
	time.sleep(t+0.01)
	y = y + 1



# Channel Name, channel , runs of loop, speed
def fade1(threadName, colour, runs , speed, maxV , minV ):
    global flags
    global multiVal
    global LEDS
    
    
    try: 
	y = 0
	z = 0.1	
	
	print ("fade program :" , threadName )
	
	while y < runs and flags[colour] == 0:
	    
		
		print(" run: " , y)
	 #runs
		y = y + 1 # stepcounter
	        print(z)
		print("fade up")
		while z < maxV  and  flags[colour] == 0:  #fade up
			print(z)
			
			z = ((z + valrgb))  #RGB value (0-255) per run * multiplier
			changer(LEDS[colour], z)
			time.sleep(speed)
			print ("fade")
		
		print("fade down") 
		while z > minV and flags[colour] == 0:# 1 rgv val
			z = ((z - valrgb))
			changer(LEDS[colour], z)	
			time.sleep(speed)	
	blackout()  
# End program cleanly with keyboard
    except flags[colour] == 1:
	print " flags 1"
	blackout()


			


