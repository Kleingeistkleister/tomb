#!/usr/bin/env
import csv
import os
import time
import sys
import RPi.GPIO as GPIO
import pygame
import thread
import threading


#GPIO OUT

#motor
MD = 5  #motor up
MU = 6	#motor down

#relais
SM = 8	#smoke
WL = 7	#white LED


#GPIO IN
SU = 12	#sensor up
SD = 13	#sensor down
BU = 19	#button



#variabels
x = 0
y = 0
z = 0
t = 0
s = 0
valrgb = 0.003921 #  % per RGB Value (0-255) 



#GPIO setup

GPIO.setmode(GPIO.BCM)

pinOutList = [5, 6,  7, 8 , 9   ] 
pinInList = [SU , SD , BU ] 


for i in pinOutList: 
	GPIO.setup(i, GPIO.OUT)
	GPIO.output(i, 0) #all gpio low

      
for i in pinInList: 
	GPIO.setup(i, GPIO.IN)
	GPIO.setup(i, GPIO.IN, pull_up_down=GPIO.PUD_UP) # pullup resistors



#sound
# initialize pygame.mixer
pygame.mixer.init(frequency = 44100, size = -16, channels = 4, buffer = 4**12)
# init() channels refers to mono vs stereo, not playback Channel object

# create separate Channel objects for simultaneous playback
scream = pygame.mixer.Sound("scream.ogg")
door = pygame.mixer.Sound("door.wav")		    




#flags to stop thread
rFlag = 0   #shared between Thread_A and Thread_B
gFlag = 0
bFlag = 0
flags = [ rFlag , gFlag , bFlag ]
valrgb = 0.003921 #  % per RGB Value (0-255) 

#RGB Multiplier
rVal = 1
gVal = 1
bVal = 1
multiVAL = [rVal, gVal, bVal]
speed = 0.01  #10 ms

#LED
b = 22
r = 23
g = 24

LEDS = [r , g , b] 


def changer(pin, amount):
    f = open('/dev/pi-blaster', 'w')
    f.write('%d=%s\n'%(pin, str(amount))) 

def colour(r , g , b):
    changer(LEDS[0], r)
    changer(LEDS[1], g)
    changer(LEDS[2], b)
    

    
    
def blackout():
    colour(0 , 0 ,0 )
    rFlag = 1   #shared between Thread_A and Thread_B
    gFlag = 1
    bFlag = 1

    
	    

def strobe(f , k ):
    smoke(4)
    y = 0
    while y < f:
	colour(0 ,0 ,0)
	time.sleep(k)
	colour(1, 1, 1)
	time.sleep(k+0.01)
	y = y + 1
	print(f -y)
	


# Channel Name, channel , runs of loop, speed
def fade1(threadName, colour, runs , speed, minV , maxV ):
    global flags
    global multiVal
    global LEDS
    lock = threading.Lock()
    lock.acquire()
    try: 
	flags[colour] = 0
	y = 0
	z = 0.1	
	
	print ("fade program :" , threadName )
	
	while y < runs and flags[colour] == 0:
	    
	    y = y + 1 # stepcounter
	    while z < maxV  and  flags[colour] == 0:  #fade up
		    			
		    z = ((z + valrgb))  #RGB value (0-255) per run * multiplier
		    changer(LEDS[colour], z)
		    time.sleep(speed)
		
		
		
	    while z > minV and flags[colour] == 0:# 1 rgv val
		    
		    z = ((z - valrgb))
		    changer(LEDS[colour], z)	
		    time.sleep(speed)	
	
    finally:
	lock.release()



			



def smoke(t):
    GPIO.output(SM, GPIO.HIGH)
    time.sleep(t)
    GPIO.output(SM , GPIO.LOW)

def up():
		print "up"
		
		while GPIO.input(SU) != 0:
			GPIO.output(MU, GPIO.HIGH)
			print "running up"
		GPIO.output(MU, GPIO.LOW)		
		


def down():
		print "down"
		GPIO.output(MD, GPIO.HIGH)		
		while GPIO.input(SD) != 0:
			print "running down"
		GPIO.output(MD, GPIO.LOW)

def create_sound(name , vol):
    fullname =  name     # path + name of the sound file
    sound = pygame.mixer.Sound(fullname)
    sound.set_volume(vol)
    return sound


# Create two threads as follows


# main loop


try:
	blackout()
			      # fade1, threadName, colour, runs , speed, minV , maxV 
	thread.start_new_thread(fade1, ("red", 0 , 10000 , 0.12, 0.5, 1))
	thread.start_new_thread(fade1, ("green", 1 , 10000 , 0.12 , 0.01, 0.03))
	while True:
		GPIO.output(WL, GPIO.HIGH)
		time.sleep(10)
		
		smoke(6.5)
		blackout()
		
		colour(1, 1 , 1)
                if True: #(GPIO.input(BU) == 0): # if button pressed
		    time.sleep(2.5)
		    if True: #(GPIO.input(BU) == 0):
			blackout()
			GPIO.output(WL, GPIO.LOW)
		    	thread.start_new_thread(fade1, ("red", 0 , 10000 , 0.15, 0.5, 1))
			smoke(0.1)
			time.sleep(5) 
			down()
			blackout()
			
			thread.start_new_thread(fade1, ("red", 0 , 10000 , 0.15, 0.2, 0.7))
			smoke(2.4)		    
			time.sleep(5)
			print("going up")
			
			
			up()
			GPIO.output(WL, GPIO.HIGH)
			blackout()
			strobe(180, 0.04)
			
			blackout()

  
# End program cleanly with keyboard
except KeyboardInterrupt:
  print " Quit"

  # Reset GPIO settings
  GPIO.cleanup()
  blackout()
  
 
